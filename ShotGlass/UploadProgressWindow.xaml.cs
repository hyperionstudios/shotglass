﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ShotGlass
{
    /// <summary>
    /// Interaction logic for UploadProgress.xaml
    /// </summary>
    public partial class UploadProgressWindow : Window
    {
        public UploadProgressWindow(Window window)
        {
            InitializeComponent();
            this.Owner = window;
            if (window != null)
            {
                this.Top = window.ActualHeight - this.Height;
                this.Left = window.ActualWidth - this.Width;
                
            }
        }

        public void SetProgress(double progress)
        {
            uploadProgressBar.Value = progress;
            if (progress >= 1)
            {
                Console.WriteLine("hooray!");
            }
        }

        private void copyButton_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.SetText(this.urlTextBox.Text);
            if (this.Owner != null && this.Owner is MainWindow)
            {
                MainWindow win = (MainWindow)this.Owner;
                win.TrayBallonTip(400, "", "URL Copied.", System.Windows.Forms.ToolTipIcon.Info);
            }
        }
    }
}
