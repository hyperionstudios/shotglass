﻿using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.FtpClient;
using System;
using System.Text;
using System.Threading;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using ShotGlass.UploadHandlers;

namespace ShotGlass
{
    /*
     * Class that will handle the screenshot
     */
    public class ImageHandler
    {
        const string allowedFileExt = "^.+.([^.]{3,10})$"; //Regex to get the file extentstion.
        private MainWindow window;

        private Bitmap screenBitmap;

        private Bitmap bitmap;
        private ImageFormat imageFormat;

        private UploadHandler uploader;

        public ImageHandler(MainWindow window)
        {
            this.window = window;;
        }

        #region Properties
        public MainWindow MainWindow
        {
            get { return window; }
        }
        public Bitmap ScreenBitmap
        {
            get { return screenBitmap; }
        }
        public Bitmap Bitmap
        {
            get { return bitmap; }
        }
        public ImageFormat ImageFormat
        {
            get { return imageFormat; }
        }
        public UploadHandler Uploader
        {
            get { return uploader; }
        }
        #endregion

        public static BitmapSource GetBitmapSource(Bitmap bitmap)
        {
            BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap
            (
                bitmap.GetHbitmap(),
                IntPtr.Zero,
                System.Windows.Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions()
            );

            return bitmapSource;
        }

        public Bitmap GetFullScreen(Size size)
        {
            //Size size = new Size((int)System.Windows.SystemParameters.VirtualScreenWidth, (int)System.Windows.SystemParameters.VirtualScreenHeight);
            screenBitmap = new Bitmap(size.Width, size.Height);
            using (Graphics graphics = Graphics.FromImage(screenBitmap))
            {
                graphics.CopyFromScreen(new Point(0,0), new Point(0, 0), size);

                return screenBitmap;

                /*Screen[] screens = Screen.AllScreens;
                for (int i = 0; i < screens.Length; i++)
                {
                    Rectangle r = screens[i].Bounds;
                
                } */
            }
        }

        public Bitmap GetScreenShot(Point source, Size size)
        {
            bitmap = new Bitmap(size.Width, size.Height);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawImage(screenBitmap, new Rectangle(0, 0, size.Width, size.Height), 
                    new Rectangle(source, size), GraphicsUnit.Pixel);

                return bitmap;
            }
        }

        public void SaveBitmap(Bitmap bitmap, string file, ImageFormat format)
        {
            if (bitmap != null || file != null)
            {
                if (format == null) 
                {   //Let's find what format we shall use.
                    Regex regex = new Regex(allowedFileExt,RegexOptions.IgnoreCase);
                    string[] ext = regex.Split(file); //index one "ext[1]" should have the ext.
                    if (ext.Length >= 2)
                    {
                        switch (ext[1].ToLower())
                        {
                            case "png":
                                format = ImageFormat.Png;
                                break;
                            case "bmp":
                                format = ImageFormat.Bmp;
                                break;
                            case "jpg":
                            case "jpeg":
                                format = ImageFormat.Jpeg;
                                break;
                            case "gif":
                                format = ImageFormat.Gif;
                                break;
                            case "tiff":
                                format = ImageFormat.Tiff;
                                break;
                            case "ico":
                                format = ImageFormat.Icon;
                                break;
                        }
                    }
                    if (format == null)
                    {
                        //Default to png type 
                        format = ImageFormat.Png;
                    }
                }
                bitmap.Save(File.OpenWrite(file), format);
                bitmap.Dispose();
            }
        }

        public void UploadImage(Bitmap bitmap, ImageFormat format)
        {
            if (format == null) { format = ImageFormat.Png; }
            this.bitmap = bitmap;
            this.imageFormat = format;
            if (Properties.Settings.Default.ftpEnable)
            {
                uploader = new FtpHandler(this);
                uploader.Upload();
            }
        }

        public void Dispose()
        {
            screenBitmap.Dispose();
            //bitmap.Dispose();
            uploader = null;
        }

        

    }

}
