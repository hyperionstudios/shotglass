﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.Drawing;

namespace ShotGlass
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow : Window
    {
        public SettingsWindow(Window window)
        {
            InitializeComponent();
            this.Owner = window;
            /* About info */
            this.labelProductName.Content = AssemblyProduct;
            this.labelVersion.Content = String.Format("Version {0}", AssemblyVersion);
            this.labelCopyright.Content = AssemblyCopyright;
            this.labelCompanyName.Content = AssemblyCompany;
            this.textBoxDescription.Text = AssemblyDescription;

            /* ftp info */
            this.ftpEnableCheckBox.IsChecked = Properties.Settings.Default.ftpEnable;
            this.ftpHostNameTextBox.Text = Properties.Settings.Default.ftpHostName;
            this.ftpPortTextBox.Text = Properties.Settings.Default.ftpPort.ToString();
            this.ftpDirectoryTextBox.Text = Properties.Settings.Default.ftpDirectory;
            this.ftpUrlTextBox.Text = Properties.Settings.Default.ftpUrlResult;
            this.ftpUsernameTextBox.Text = Properties.Settings.Default.ftpUsername;
            this.ftpPasswordBox.Password = Properties.Settings.Default.ftpPassword;

            /* general */
            FillColors(selectionColorComboBox);
            SetComboBoxColor(selectionColorComboBox, Properties.Settings.Default.SelectionColor);
            
        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        #region Color Comboboxes
        public Color GetComboBoxColor(ComboBox box)
        {
            if (box.SelectedItem != null)
                return (Color)box.SelectedItem;
            return Color.DarkCyan;
        }
        public void SetComboBoxColor(ComboBox box, Color color)
        {                 
            int ix = box.Items.IndexOf(color);
            if (ix >= 0) {
                box.SelectedIndex = ix;
            }
        }

        private void FillColors(ComboBox box)
        {
            box.Items.Clear();
            // Fill Colors using Reflection
            foreach (Color color in typeof(Color).GetProperties(BindingFlags.Static | BindingFlags.Public).Where(c => c.PropertyType == typeof(Color)).Select(c => (Color)c.GetValue(c, null)))
            {
                box.Items.Add(color);
            }  
        }
        #endregion

        #region gui events
        private void ftpPortTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            short result;
            if (short.TryParse(e.ControlText, out result)) //force only short numbers.
            {
                e.Handled = true;
            }
        }
        #endregion


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (sender == okButton)
            {
                Properties.Settings.Default.SelectionColor = GetComboBoxColor(selectionColorComboBox);
                Properties.Settings.Default.ftpHostName = ftpHostNameTextBox.Text;
                short port = 21;
                if (short.TryParse(ftpPortTextBox.Text, out port))
                {
                    Properties.Settings.Default.ftpPort = port;
                }
                Properties.Settings.Default.ftpDirectory = ftpDirectoryTextBox.Text;
                Properties.Settings.Default.ftpUrlResult = ftpUrlTextBox.Text;
                Properties.Settings.Default.ftpUsername = ftpUsernameTextBox.Text;
                Properties.Settings.Default.ftpPassword = ftpPasswordBox.Password;
                Properties.Settings.Default.ftpEnable = //if ischecked is null or false. Set as false. (weird implementation....) 
                    ftpEnableCheckBox.IsChecked == false || ftpEnableCheckBox.IsChecked == null ? false : true;

                Properties.Settings.Default.Save();
                MainWindow win = this.Owner as MainWindow;
                if (win != null)
                {
                    win.TrayBallonTip(400, "", "Settings have been saved.", System.Windows.Forms.ToolTipIcon.Info);
                }
                this.Close();
            }
            else if (sender == cancelButton)
            {
                this.Close();
            }
        }

    }
}
