﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Interop;
using System.Windows.Media.Effects;
using System.Drawing.Imaging;
using System;
using System.Windows.Media.Imaging;
using ShotGlass.UploadHandlers;

namespace ShotGlass
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int imageOffsetX = 32, imageOffsetY = 32;
        private Point source;
        private Point destination;

        private Rect copyRectangle;

        private bool editMode;

        private ImageHandler imageHandler;

        /* tray */
        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.ContextMenu trayMenu;

        /* windows */

        private SettingsWindow settingsWindow;
        private UploadProgressWindow uploadProgressWindow;

        public MainWindow()
        {
            InitializeComponent();

            //Enable software rendering as large rectangles have huge performace issues.
            RenderOptions.ProcessRenderMode = RenderMode.SoftwareOnly;

            this.imageHandler = new ImageHandler(this);

            /* modify gui from settings */
            Color c = ConvertColors(Properties.Settings.Default.SelectionColor);
            selectionRect.Fill = new SolidColorBrush(c);
            c.A = 255; //remove alpha
            selectionRect.Stroke = new SolidColorBrush(c);
        }

        private void TrayInit()
        {                  
            trayMenu = new System.Windows.Forms.ContextMenu();
            
            trayMenu.MenuItems.Add(0, new System.Windows.Forms.MenuItem("Capture Screen", new System.EventHandler(trayMenuItem_MouseClick)));
            trayMenu.MenuItems.Add(1, new System.Windows.Forms.MenuItem("Settings...", new System.EventHandler(trayMenuItem_MouseClick)));
            trayMenu.MenuItems.Add(2, new System.Windows.Forms.MenuItem("Exit", new System.EventHandler(trayMenuItem_MouseClick)));

            trayIcon = new System.Windows.Forms.NotifyIcon();
            trayIcon.Icon = Properties.Resources.shotglass;
            trayIcon.Text = "ShotGlass";
            trayIcon.Visible = true;
            trayIcon.ContextMenu = trayMenu;
            trayIcon.MouseClick += new System.Windows.Forms.MouseEventHandler(trayIcon_MouseClick);

        }

        public System.Drawing.Bitmap GetImage(System.Drawing.Point point, System.Drawing.Size size)
        {
            return imageHandler.GetScreenShot(point, size);

        }
        public System.Drawing.Bitmap GetImage()
        {
            return GetImage(
                new System.Drawing.Point((int)copyRectangle.X, (int)copyRectangle.Y),
                new System.Drawing.Size((int)copyRectangle.Width, (int)copyRectangle.Height)
                );
        }


        protected void CopyImage()
        {
            TrayBallonTip(400, "", "Screenshot copied to clipboard.", System.Windows.Forms.ToolTipIcon.Info);
            Clipboard.SetImage(ImageHandler.GetBitmapSource(GetImage())); 
            this.WindowState = WindowState.Minimized;
        }
        protected void SaveImage()
        {
            // Configure open file dialog box
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "ScreenShot_1"; // Default file name
            dlg.AddExtension = true;
            dlg.DefaultExt = ".png"; // Default file extension
            dlg.Filter = "PNG|*.png|JPEG|*.jpeg;*.jpg|GIF|*.gif|BMP|*.bmp|TIFF|*.tiff"; // Filter files by extension
            dlg.OverwritePrompt = true;
            // Show open file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process open file dialog box results
            if (result == true)
            {
                // Open document
                string filename = dlg.FileName;
                imageHandler.SaveBitmap(GetImage(), filename, null);
            }
        }
        protected void UploadImage()
        {
            imageHandler.UploadImage(GetImage(), null);
            this.WindowState = WindowState.Minimized;
            uploadProgressWindow = new UploadProgressWindow(this);
            uploadProgressWindow.Show();
            //weird requiring this referencing...?
            imageHandler.Uploader.UploadProgressChange += new ShotGlass.UploadHandlers.UploadHandler.UploadProgressChangeEventHandler(OnUploadProgress); 
        }

        private void DrawRectangle(bool withButtons)
        {
            destination = Mouse.GetPosition(canvas1);
            selectionRect.Visibility = System.Windows.Visibility.Visible;

            double disX = destination.X - source.X;
            double disY = destination.Y - source.Y;

            if (disX < 0)
            {
                copyRectangle.X = destination.X;
                copyRectangle.Width = source.X - destination.X;
            }
            else
            {
                copyRectangle.X = source.X;
                copyRectangle.Width = disX;
            }

            if (disY < 0)
            {
                copyRectangle.Y = destination.Y;
                copyRectangle.Height = source.Y - destination.Y;
            }
            else
            {
                copyRectangle.Y = source.Y;
                copyRectangle.Height = disY;
            }

            selectionRect.Width = copyRectangle.Width;
            selectionRect.Height = copyRectangle.Height;
            selectionRect.RenderTransform = new TranslateTransform()
            {
                X = copyRectangle.X,
                Y = copyRectangle.Y,
            };
            
            if (withButtons)
            {
                cancelImage.Visibility = Visibility.Visible;
                cancelImage.RenderTransform = new TranslateTransform()
                {
                    X = copyRectangle.X - imageOffsetX,
                    Y = copyRectangle.Y - imageOffsetY,
                };
                copyImage.Visibility = Visibility.Visible;
                copyImage.RenderTransform = new TranslateTransform()
                {
                    X = copyRectangle.X,
                    Y = copyRectangle.Y - imageOffsetY,
                };
                saveImage.Visibility = Visibility.Visible;
                saveImage.RenderTransform = new TranslateTransform()
                {
                    X = copyRectangle.X + imageOffsetX,
                    Y = copyRectangle.Y - imageOffsetY,
                };
                uploadImage.Visibility = Visibility.Visible;
                uploadImage.RenderTransform = new TranslateTransform()
                {
                    X = copyRectangle.X + (imageOffsetX * 2),
                    Y = copyRectangle.Y - imageOffsetY,
                };
            }
            else
            {
                copyImage.Visibility = Visibility.Collapsed;
                cancelImage.Visibility = Visibility.Collapsed;
                saveImage.Visibility = Visibility.Collapsed;
                uploadImage.Visibility = Visibility.Collapsed;
            }

        }

        #region ulitity methods
        protected Color ConvertColors(System.Drawing.Color color)
        {
            Color c = Color.FromArgb(20,color.R, color.G, color.B); 
            return c;
        }
        #endregion

        #region Tray Methods
        public void TrayBallonTip(int timeout, string title, string message, System.Windows.Forms.ToolTipIcon toolTipIcon)
        {
            if (message != null)
            {
                if (timeout == 0) { timeout = 400; }
                if (title == null) { title = ""; }
                trayIcon.ShowBalloonTip(timeout, title, message, toolTipIcon);
            }
        }
        #endregion

        #region Window events
        private void Window_Initialized(object sender, EventArgs e)
        {
            TrayInit();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            trayIcon.Dispose();
        }
        private void Window_StateChanged(object sender, EventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {

                this.Background = new ImageBrush(ImageHandler.GetBitmapSource(
                    imageHandler.GetFullScreen(
                        new System.Drawing.Size((int)this.Width-8,(int)this.Height-5)
                    )));
            }
            else
            {
                imageHandler.Dispose();
            }
        }

        #endregion

        #region GUI events
        private void canvas1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                DrawRectangle(false);
            }
        }

        private void canvas1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                source = Mouse.GetPosition(canvas1);
            }
        }

        private void canvas1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ButtonState == MouseButtonState.Released)
            {
                DrawRectangle(true);
                editMode = true;
            }
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ButtonState == MouseButtonState.Released)
            {
                if (sender == cancelImage)
                {   //Cancel event
                    this.Close();
                }
                else if (sender == copyImage)
                { //Copy
                    CopyImage();
                }
                else if (sender == saveImage)
                {
                    SaveImage();
                }
                else if (sender == uploadImage)
                {
                    UploadImage();   
                }
            }
        }
        #endregion

        #region Special Effects
        /* visual effects only! */
        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            if (image != null)
            {
                DropShadowEffect effect = new DropShadowEffect()
                {
                  BlurRadius = 3,
                  Color = Color.FromRgb(0,0,255),
                  Direction = 0.3,
                  RenderingBias = RenderingBias.Quality,
                  ShadowDepth = 0.5,
                }; 
                image.Effect = effect;
            }
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            Image image = sender as Image;
            if (image != null)
            {
                image.Effect = null;
            }
        }

        #endregion

        #region Tray events

        private void trayIcon_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
            }
               
        }
        private void trayMenuItem_MouseClick(object sender, EventArgs e)
        {
            System.Windows.Forms.MenuItem m = sender as System.Windows.Forms.MenuItem;
            if (m != null)
            {
                if (m.Index == 0) //Capture screen
                { 
                    this.WindowState = System.Windows.WindowState.Maximized;
                }
                else if (m.Index == 1) //Settings
                {
                    settingsWindow = new SettingsWindow(this);
                    settingsWindow.Show();
                    
                }
                else if (m.Index == 2) //Exit
                {
                    this.Close();
                }
            }
        }     
	    #endregion

        #region Upload Events
        protected void OnUploadProgress(object sender, UploadEventArgs e)
        {
            uploadProgressWindow.SetProgress(e.UploadProgress);
        }
        #endregion


    }
}
