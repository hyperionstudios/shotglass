﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShotGlass.UploadHandlers
{
    public abstract class UploadHandler
    {

        protected ImageHandler imageHandler;
        private double uploadProgress;

        public delegate void UploadCompleteEventHandler(object sender, UploadEventArgs e);
        public delegate void UploadFailedEventHandler(object sender, UploadEventArgs e);
        public delegate void UploadProgressChangeEventHandler(object sender, UploadEventArgs e);

        public event UploadCompleteEventHandler UploadComplete;
        public event UploadFailedEventHandler UploadFailed;
        public event UploadProgressChangeEventHandler UploadProgressChange;

        public UploadHandler(ImageHandler imageHandler)
        {
            this.imageHandler = imageHandler;
        }

        public virtual double UploadProgress
        {
            get { return uploadProgress; }
            protected set {
                uploadProgress = value;
                if (uploadProgress >= 1)
                {
                    uploadProgress = 1;
                    UploadCompleted = true;
                }
            }
        }
        public virtual bool UploadCompleted
        {
            get;
            protected set;
        }


        protected void OnUploadComplete(UploadEventArgs e)
        {
            if (UploadComplete != null)
                UploadComplete(this, e);
        }
        protected void OnUploadProgressChange(UploadEventArgs e)
        {
            if (UploadProgressChange != null)
                UploadProgressChange(this, e);
        }
        protected void OnUploadFailed(UploadEventArgs e)
        {
            if (UploadFailed != null)
                UploadFailed(this, e);
        }
        public virtual void Upload() { }
    }
}
