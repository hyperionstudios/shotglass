﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ShotGlass.UploadHandlers
{
    public class UploadEventArgs : EventArgs
    {

        public UploadEventArgs(double uploadProgress, bool uploadCompleted)
        {
            UploadProgress = uploadProgress;
            UploadCompleted = uploadCompleted;
        }

        public double UploadProgress
        {
            get;
            private set;
        }
        public bool UploadCompleted
        {
            get;
            private set;
        }
    }
}
