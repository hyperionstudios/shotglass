﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.FtpClient;
using System.IO;
using System.Net;

namespace ShotGlass.UploadHandlers
{
    public class FtpHandler : UploadHandler
    {
        private FtpClient ftpClient;

        public FtpHandler(ImageHandler imageHandler) : base(imageHandler)
        {
            ftpClient = new FtpClient();
        }

        #region properties
        public FtpClient FtpClient
        {
            get { return ftpClient; }
        }
        #endregion

        public override void Upload()
        {
            ConnectFTP(ftpClient);

            //Generate a random name.
            string file = GenerateRandomFileName(ftpClient);

            AsyncCallback callback = new AsyncCallback(UploadCallBack);;
            ftpClient.BeginOpenWrite(file, callback, ftpClient);
        
        }

        #region ftp
        private void ConnectFTP(FtpClient ftpClient)
        {
            try
            {
                ftpClient.Host = Properties.Settings.Default.ftpHostName;
                ftpClient.Port = Properties.Settings.Default.ftpPort;
                ftpClient.Credentials = new NetworkCredential(Properties.Settings.Default.ftpUsername, Properties.Settings.Default.ftpPassword);

                ftpClient.Connect();

                if (!ftpClient.DirectoryExists(Properties.Settings.Default.ftpDirectory)) //create directory if it doesn't exist.
                {
                    ftpClient.CreateDirectory(Properties.Settings.Default.ftpDirectory, true);
                }
                ftpClient.SetWorkingDirectory(Properties.Settings.Default.ftpDirectory);

            }
            catch (Exception e)
            {
                Console.WriteLine("FTP has failed to connect. " + e.Message);
            }
        }

        private void UploadCallBack(IAsyncResult result)
        {
            FtpClient ftpClient = result.AsyncState as FtpClient;


            if (ftpClient != null)
            {
                byte[] buff = new byte[1024];//8192];

                int read = 0;
                using (Stream outStream = ftpClient.EndOpenWrite(result), inStream = new MemoryStream())
                 {
                    try
                    {
                        imageHandler.Bitmap.Save(inStream, imageHandler.ImageFormat);
                        inStream.Position = 0;
                        long readed = 0, length = inStream.Length; 
                        while ((read = inStream.Read(buff, 0, buff.Length)) > 0)
                        {
                            outStream.Write(buff, 0, read);
                            readed += read;
                            UploadProgress = (readed - length) / length; // progress percent
                            OnUploadProgressChange(new UploadEventArgs(UploadProgress, UploadCompleted));

                        }
                        inStream.Close();
                        outStream.Close();                      

                        UploadCompleted = true;
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("FTP upload has failed. " + e.Message +" "+ e.StackTrace);
                        OnUploadFailed(new UploadEventArgs(UploadProgress, UploadCompleted));
                    }
                    finally
                    {
                        if (inStream != null)
                            inStream.Close();
                        if (outStream != null)
                            outStream.Close();
                    }
                }
                ftpClient.Disconnect();
                ftpClient.Dispose();

                /* report complete */
                if (UploadCompleted)
                    OnUploadComplete(new UploadEventArgs(UploadProgress, UploadCompleted));
            }
        }

        private string GenerateRandomFileName(FtpClient ftpClient)
        {
            //Attempt to generate a random string formulated by 10 random numbers then append the millisecond.
            //Incase an exisitng file is there, keep trying.
            Random r = new Random();
            string file = ""; 
            StringBuilder sb = new StringBuilder();
            do
            {   
                sb.Clear();
                for (int i = 0; i < 10; i++)
                {
                    sb.Append(r.Next(9));
                }
                sb.Append("_");
                sb.Append(DateTime.Now.Millisecond);
                sb.Append(".png");
                file = sb.ToString();
            }
            while (ftpClient.FileExists(file));
            return file;
        }
        #endregion

    }
}
